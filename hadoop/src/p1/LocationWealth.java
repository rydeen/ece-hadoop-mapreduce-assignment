package p1;

import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;



public class LocationWealth {

	/**
	 * MAP class
	 * @author luisabreu
	 */
	public static class Map extends MapReduceBase 
		implements Mapper<LongWritable, Text, Text, IntWritable>
	{
		// will be used as the delimiter to break the input into lines
		private String line_delimiter = "\n" ;
		// will be used as the delimiter to break each line into words
		private String field_delimiter = ";" ;
		
		private Text line = new Text() ;
		private Text word_key = new Text() ;
		private IntWritable word_value = new IntWritable();

		@SuppressWarnings("unused")
		@Override
		public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, 
				Reporter reporter) throws IOException {
			
			String file = value.toString() ;
			StringTokenizer line_tokenizer = new StringTokenizer(file, line_delimiter) ;
			
			// for each line in the input file
			while( line_tokenizer.hasMoreTokens() ) {
				
				// each line in the file is in the form
				// name ; location ; age ; account_balance ; number_of_friends
				line.set( line_tokenizer.nextToken() );
				StringTokenizer field_tokenizer = new StringTokenizer(line.toString(), field_delimiter) ;
				
				// different values extracted from the current line of the input file
				String name = field_tokenizer.nextToken() ;
				String location = field_tokenizer.nextToken() ;
				String age = field_tokenizer.nextToken() ;
				String account_balance = field_tokenizer.nextToken() ;
				String n_friends = field_tokenizer.nextToken() ;
				
				
				
				// use location as 'key' and account_balance as 'value'
				word_key.set( location );
					
				// if the 'account_balance' is not a parsable int, ignore
				// this happens in the first line of the csv file: the header
				// this can also be triggered if the account balance is not a valid integer
				boolean ignore = false ;
				
				if ( word_key.equals("city")) //value from the header
					ignore = true ;
				
				try {
					word_value.set( Integer.parseInt(account_balance) ); 
				}
				catch( NumberFormatException e ) {
					ignore = true ;
				}
				
				// only collect valid entries
				if ( ! ignore )
					output.collect(word_key, word_value);
			}	
		}
	}
	
	/**
	 * REDUCE class
	 * @author luisabreu
	 */
	public static class Reduce extends MapReduceBase 
		implements Reducer<Text, IntWritable, Text, IntWritable>
	{

		@Override
		public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> output,
				Reporter report) throws IOException {
			
			int sum = 0 ;
			
			// sum the account_balances from the same locations
			while ( values.hasNext() ) 
				sum += values.next().get() ;
				
			output.collect( key , new IntWritable(sum) );
		}
	}

	public static void main( String [] args ) throws Exception {
		
		// check if the users provided two arguments or not
		if ( args.length != 2 )
		{
			help() ;
			return ;
		}

		System.out.println("\n- input   >> " + args[0]);
		System.out.println("- output  >> " + args[1]);
		System.out.println("- running >> locationwealth\n");
		
		// create 'Path' objects with strings providade by the user
		Path input = new Path(args[0]) ;
		Path output = new Path(args[1]) ;
		
		JobConf conf = new JobConf(LocationWealth.class) ;
		conf.setJobName("locationwealth");
		
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(IntWritable.class);
		
		conf.setMapperClass(Map.class);
		conf.setCombinerClass(Reduce.class);
		conf.setReducerClass(Reduce.class);
		
		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);
		
		// output as csv file
		conf.set("mapred.textoutputformat.separator", ";");
				
		// set the paths to input and output
		FileInputFormat.setInputPaths(conf, input );
		FileOutputFormat.setOutputPath(conf, output);
		
		JobClient.runJob( conf ) ;
	}

	/**
	 * the 'help' function is used to teach the user on how to use the application
	 * this functions should be called every time the user makes a wrong call.
	 */
	protected static void help() {
		System.out.println("\nWrong number of arguments provided!");
		System.out.println("Usage: locationwealth <input path> <output path>");
		System.out.println("\nTry again.\n");
	}

}
