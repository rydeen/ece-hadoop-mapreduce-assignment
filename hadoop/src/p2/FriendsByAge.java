package p2;

import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;



public class FriendsByAge {

	/**
	 * MAP class
	 * @author luisabreu
	 */
	public static class Map extends MapReduceBase 
		implements Mapper<LongWritable, Text, Text, FloatWritable>
	{
		// will be used as the delimiter to break the input into lines
		private String line_delimiter = "\n" ;
		// will be used as the delimiter to break each line into words
		private String field_delimiter = ";" ;
		
		private Text line = new Text() ;
		
		// key and value to send to output
		private Text word_key = new Text() ;
		private FloatWritable word_value = new FloatWritable();

		
		/**
		 * This functions is used to determine the key, based on the value of 'value'
		 * @param value
		 * @return Text. The key to map the values to output
		 * If the 'value' is not a parsable integer, the key will be "unknown"
		 */
		private Text determineGroupKey( String value )
		{
			int aux ;
			try {
				aux = Integer.parseInt( value ) ;
			} catch( NumberFormatException e ) {
				aux = -1 ;
			}
			
			Text groupkey = new Text() ;
			
			// if-else chain to determine the range in which 'aux' can be represented
			if ( aux>=0 && aux<=5 )
				groupkey.set("0-5 years");
			else if ( aux>=6 && aux<=12 )
				groupkey.set("6-12 years");
			else if ( aux>=13 && aux<=17 )
				groupkey.set("13-17 years");
			else if ( aux>=18 && aux<=25 )
				groupkey.set("18-25 years");
			else if ( aux>=26 && aux<=35 )
				groupkey.set("26-35 years");
			else if ( aux>=36 && aux<=45 )
				groupkey.set("36-45 years");
			else if ( aux>=46 && aux<=60 )
				groupkey.set("46-60 years");
			else if ( aux>60 )
				groupkey.set("60+ years");
			else
				groupkey.set("unknown");
			
			return groupkey ;
		}

		@SuppressWarnings("unused")
		@Override
		public void map(LongWritable key, Text value, OutputCollector<Text, FloatWritable> output, 
				Reporter reporter) throws IOException {
			
			String file = value.toString() ;
			StringTokenizer line_tokenizer = new StringTokenizer(file, line_delimiter) ;
			
			// for each line in the input file
			while( line_tokenizer.hasMoreTokens() ) {
				
				// each line in the file is in the form
				// name ; location ; age ; account_balance ; number_of_friends
				line.set( line_tokenizer.nextToken() );
				StringTokenizer field_tokenizer = new StringTokenizer(line.toString(), field_delimiter) ;
				
				// different values extracted from the current line of the input file
				String name = field_tokenizer.nextToken() ;
				String location = field_tokenizer.nextToken() ;
				String age = field_tokenizer.nextToken() ;
				String account_balance = field_tokenizer.nextToken() ;
				String n_friends = field_tokenizer.nextToken() ;
				
				// use age range as 'key' and n_friends as 'value'
				// the function determineGroupKey() will determine the 'age range'
				word_key.set( determineGroupKey( age ) );


				// if the 'n_friends' is not a parsable float, this entry should be ignored.
				// otherwise it will influence the results.
				// this happens in the first line of the csv file: the header
				boolean ignore = false ;
				
				if ( word_key.equals("unknown") ) //'unknown' is the value returned by 'determineGroupKey()' invalid inputs
					ignore = true ;
				
				try {
					word_value.set( Float.parseFloat( n_friends ) ); 
				} catch (NumberFormatException e) {
					ignore = true ;
				}
				
				if ( ! ignore )
					output.collect(word_key, word_value);
			}	
		}
	}
	
	/**
	 * REDUCE class
	 * @author luisabreu
	 */
	public static class Reduce extends MapReduceBase 
		implements Reducer<Text, FloatWritable, Text, FloatWritable>
	{
		static float valuesCounter = 0f ;
		
		@Override
		public void reduce(Text key, Iterator<FloatWritable> values, OutputCollector<Text, 
				FloatWritable> output, Reporter report) throws IOException 
		{	
			float sum = 0 ;
			
			// sum the number of friends by age range
			while ( values.hasNext() )
			{
				sum += values.next().get() ;
				valuesCounter+=1;
			}
			
			float avarage = sum / valuesCounter ;
			
			output.collect( key , new FloatWritable(avarage) );
		}
	}

	public static void main( String [] args ) throws Exception {
		
		// check if the users provided two arguments or not
		if ( args.length != 2 )
		{
			help() ;
			return ;
		}
		
		System.out.println("\n- input   >> " + args[0]);
		System.out.println("- output  >> " + args[1]);
		System.out.println("- running >> friendsbyage\n");
		
		// create 'Path' objects with strings providade by the user
		Path input = new Path(args[0]) ;
		Path output = new Path(args[1]) ;
		
		JobConf conf = new JobConf(FriendsByAge.class) ;
		conf.setJobName("friendsbyage");
		
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(FloatWritable.class);
		
		conf.setMapperClass(Map.class);
		conf.setReducerClass(Reduce.class);
		
		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);
		
		// output as csv file
		conf.set("mapreduce.output.textoutputformat.separator", ";");
		
		// set the paths to input and output
		FileInputFormat.setInputPaths(conf, input );
		FileOutputFormat.setOutputPath(conf, output);
		
		JobClient.runJob( conf ) ;
	}

	/**
	 * the 'help' function is used to teach the user on how to use the application
	 * this functions should be called every time the user makes a wrong call.
	 */
	protected static void help() {
		System.out.println("\nWrong number of arguments provided!");
		System.out.println("Usage: friendsbyage <input path> <output path>");
		System.out.println("\nTry again.\n");
	}
	
}
