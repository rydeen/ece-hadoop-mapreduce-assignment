# MapReduce Assignment 1 #
state: *submitted to evaluation*

## Description ##
Repository create to store all the code generated to the first assignment of this course.
For this first assignment, these are the exercises.

**Practice 1**
Using MapReduce, please compute for each location, the sum of the banking accounts to determine location wealth.

**Practice 2**
Using MapReduce, please compute for each age group listed below, the average number of friend.

0-5 years; 6-12 years; 13-17 years; 18-25 years; 26-35 years; 36-45 years; 46-60 years; 60+ years


## Informations ##
* **School:** ECE Paris
* **Course:** Big Data with Hadoop
* **Professor:** Pierre Sauvage
* **Author:** Luis Pedro Borges Abreu

## Setup ##
These are the commands used to test the project in the server **hadoop-ece.tk**

First, copy the tar file containing the project to the server.

```
#!bash
tar czf hadoop.tar.gz hadoop/
```

Second, untar the file and generate the *jar* file using Maven.

```
#!bash
[borges@hadoop assignment]$ tar xzf hadoop.tar.gz 
[borges@hadoop assignment]$ cd hadoop
[borges@hadoop hadoop]$ mvn clean package
```


Inside the *jar* file there are two different processes. One to run the first exercise and the other to run the second exercise.

The main classes are *p1.LocationWealth* (to run the first exercise) and *p2.FriendsByAge* (to run the second exercise).

These are the commands I used to execute both of them on the server:

```
#!bash
# practice 1
[borges@hadoop hadoop]$ yarn jar target/hadoop-1.0.0.jar p1.LocationWealth /user/borges/file.csv /user/borges/output1

# practice 2
[borges@hadoop hadoop]$ yarn jar target/hadoop-1.0.0.jar p2.FriendsByAge /user/borges/file.csv /user/borges/output2
```

## Run ##

After compiling with Maven, this is the "standard" command to run this project.

```
#!bash
$ yarn jar hadoop-1.0.0.jar <main-class> <input> <output>
```

Where "*<input>*" and "*<output>*" paths must be paths of the HDFS (Hadoop Distributed File System). The output folder will be created by Hadoop, so this should be a new folder to be created.

The "*<main-class>*" can be one of the followings:

* *p1.LocationWealth* - for Practice 1
* *p2.FriendsByAge* - for Practice 2

## Results ##

These are the results obtained for the practice 1 and practice 2 written in *csv* format.


```
#!bash
[borges@hadoop hadoop]$ hdfs dfs -cat /user/borges/output1/part-00000
Bordeaux;15146906
Grenoble;15100664
Lille;15870332
Lyon;14500047
Marseille;15708634
Nantes;15984687
Nice;13719135
Paris;15733403
Reims;15486616
Strasbourg;14987973

[borges@hadoop hadoop]$ hdfs dfs -cat /user/borges/output2/part-00000
0-5 years;4.861
13-17 years;15.238813
18-25 years;13.104218
26-35 years;10.424622
36-45 years;3.2137616
46-60 years;3.519534
6-12 years;1.4635581
60+ years;1.9759988

```



## Questions ##
**1. Is it useful to use the reducer class as a combiner? Justify.**

The reducer class can be used as a combiner when the function we want to apply is both associative and commutative. 
In the first exercise, I have and addition function, and so it respects the these two properties.
So, in this case it is useful and it's being used. 
On the other hand, in the second exercise we want to compute average values, and using the reducer class as a combiner can result in unexpected results! Thats why in the second exercise, 
I didn't use the reducer class as a combiner. The average function is not both associative and commutative.